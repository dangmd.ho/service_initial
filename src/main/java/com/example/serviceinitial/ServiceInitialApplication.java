 package com.example.serviceinitial;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

 @SpringBootApplication
public class ServiceInitialApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ServiceInitialApplication.class, args);
	}
	 @Override
	 public void run(String... args) throws Exception {
		 // Do nothing because of X and Y.
	 }

	 @Bean
	 public CorsFilter corsFilter() {
		 UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		 CorsConfiguration config = new CorsConfiguration();
		 config.setAllowCredentials(true); // you USUALLY want this
		 config.addAllowedOrigin("*");
		 config.addAllowedHeader("*");
		 config.addAllowedMethod("*");

		 source.registerCorsConfiguration("/**", config);
		 return new CorsFilter(source);
	 }

	 @Bean
	 public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
		 RedisTemplate<String, Object> template = new RedisTemplate<>();
		 template.setConnectionFactory(connectionFactory);
		 return template;
	 }


}
