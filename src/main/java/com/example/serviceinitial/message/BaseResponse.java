package com.example.serviceinitial.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse {
    @Builder.Default
    private String message = "OK";

    @Builder.Default
    private LocalDateTime timestamp = LocalDateTime.now(ZoneOffset.UTC);

    private String errorCode;

    private Object data;

    public BaseResponse(Object data) {
        this.data = data;
    }
}
