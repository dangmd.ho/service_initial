package com.example.serviceinitial.exception;

import com.example.serviceinitial.message.BaseResponse;
import com.example.serviceinitial.message.ErrorMessage;
import com.example.serviceinitial.message.ErrorModel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        String error = "Malformed JSON request ";

        BaseResponse response = BaseResponse.builder()
                .errorCode("BAD_DATA")
                .message(error + ex.getMessage())
                .build();

        return new ResponseEntity<>(response, status);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        ErrorModel errorModel = new ErrorModel();
        errorModel.setHttpStatus((HttpStatus) status);
        errorModel.setTimestamp(LocalDateTime.now(ZoneOffset.UTC));

        ErrorMessage errorMessage = null;
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            errorMessage = ErrorMessage.builder()
                    .fieldName(fieldError.getField())
                    .errorMessage(fieldError.getDefaultMessage())
                    .build();

            errorModel.getDetails().add(errorMessage);
        }

        BaseResponse baseResponse = BaseResponse.builder()
//				.statusCode(status)
                .message(HttpStatus.BAD_REQUEST.name())
                .data(errorModel.getDetails())
                .build();

        return new ResponseEntity<Object>(baseResponse, HttpStatus.BAD_REQUEST);
    }



    @ExceptionHandler(Exception.class)
    protected ResponseEntity <Object> handleCustomAPIException(Exception ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        BaseResponse response = BaseResponse.builder()
//				.statusCode(status)
                .errorCode(status.BAD_GATEWAY.name())
                .message("Something went wrong: " + ex.getMessage())
                .build();

        return new ResponseEntity<>(response, status);
    }
}
